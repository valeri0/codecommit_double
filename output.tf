output codecommit_service_arn {
  value = "${aws_codecommit_repository.service.arn}"
}

output repository_service_name {
  value = var.repository_name
}
output codecommit_deploy_arn {
  value = "${aws_codecommit_repository.deploy.arn}"
}

output repository_deploy_name {
  value = "${var.repository_name}-deploy"
}
