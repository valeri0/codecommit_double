resource "aws_codecommit_repository" "service"  {
  repository_name = var.repository_name
  tags =  var.repository_tag
}
resource "aws_codecommit_repository" "deploy"  {
  repository_name = "${var.repository_name}-deploy"
  tags =  var.repository_tag
}

